package com;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Palabra {
    public static void main(String... args){
        String linea="";
        
        DataInputStream in = null;
        BufferedReader br = null;
        StringTokenizer st = null;
        FileInputStream fis = null;
        LinkedList words = null;
        LinkedList counter = new LinkedList();
          
        try{
            fis = new FileInputStream("test.txt");
            in = new DataInputStream(fis);
            br = new BufferedReader(new InputStreamReader(in));
            st = null;
            
            words=new LinkedList();
            int j=0;
            String test="";
            
            while((linea = br.readLine().trim()) != null){
                st = new StringTokenizer(linea);
                j=st.countTokens();
                for(int i=0;i<j;i++){
                    test=st.nextToken();               
                    if(words.contains(test)==false){
                        words.add(test);
                        counter.add(1); 
                    }else{
                        int temp= (int)counter.get(words.indexOf(test));
                        counter.remove(words.indexOf(test));
                        counter.add(words.indexOf(test), temp+1);
                    }
                }
                
            }      
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if(fis!=null){
                try {
                    in.close();
                    br.close();
                    fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(Palabra.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        
       File archivo = new File("output.txt");
       try { 
           FileWriter escribirArchivo = new FileWriter(archivo, true);
           BufferedWriter buffer = new BufferedWriter(escribirArchivo);
           buffer.write("Total de palabras: "+words.size());
           buffer.newLine();
           for(int w=0;w<counter.size();w++){
                buffer.write("Palabra "+(w+1)+":"+words.get(w)+"\t Repeticiones: "+counter.get(w));
                buffer.newLine();
           }
           
           buffer.close();
           System.out.println("Archivo Creado Exitosamente ^^");
       }catch(Exception ex){ 
       } 
    }
}